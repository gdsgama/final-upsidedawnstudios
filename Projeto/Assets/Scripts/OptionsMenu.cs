﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public Slider musicSlider;
    public Slider fxSlider;
    public Button tick;
    public Button tickVibrateButton;

    bool ticked;
    bool tickedVib;

    void Start()
    {
        ticked = !PlayerPrefs.GetInt("showCoins").Equals(1);
        tickedVib = !PlayerPrefs.HasKey("vibrate");
        clickTick();
        clickTickVibrate();
        musicSlider.value = PlayerPrefs.GetFloat("musicVolume");
        fxSlider.value = PlayerPrefs.GetFloat("fxVolume");
    }

    public void ClickToExitOptions()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
        PlayerPrefs.SetFloat("fxVolume", fxSlider.value);
        SceneManager.LoadScene("Menu");
    }

    public void clickTick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        if (ticked)
        {
            tick.image.color = new Color32(255, 255, 255, 100);
            PlayerPrefs.SetInt("showCoins", 0);
            ticked = false;
        }
        else
        {
            tick.image.color = new Color(255, 255, 255, 255);
            PlayerPrefs.SetInt("showCoins", 1);
            ticked = true;
        }
    }
    public void clickTickVibrate()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        if (tickedVib)
        {
            tickVibrateButton.image.color = new Color32(255, 255, 255, 100);
            PlayerPrefs.DeleteKey("vibrate");
            tickedVib = false;
        }
        else
        {
            tickVibrateButton.image.color = new Color(255, 255, 255, 255);
            PlayerPrefs.SetInt("vibrate", 1);
            tickedVib = true;
        }
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Fuel : MonoBehaviour
{
    public static float fuel;

    public PlayerMovement playerScript;
    public static float valueOfDecreaseLevel;
    public Color lowColor;
    public Color fullColor;
    public Color mediumColor;
    public Image fill;

    Slider fuelSlider;
    bool flag;
    uint time, interval = 9;
    private void Awake()
    {
        flag = true;
        fuelSlider = this.GetComponent<Slider>();
    }
    void Start()
    {
        time = 0;
        fuel = 1;
        fuelSlider.value = fuel;
        valueOfDecreaseLevel = 0.00077f - 0.00009f * 2;
        //valueOfDecreaseLevel = 0.00077f - 0.00009f * PlayerPrefs.GetInt("battery");
    }

    // Update is called once per frame
    void Update()
    {
        fuelSlider.value = fuel;
        if (fuel >= 0.5f)
        {
            fill.color = Color.Lerp(mediumColor, fullColor, (fuel - 0.5f) * 2);
        }
        else if (fuel > 0)
        {
            fill.color = Color.Lerp(lowColor, mediumColor, fuel * 2);
        }
        if (fuel <= 0)
        {
            if (flag)
            {
                StartCoroutine(playerScript.waitForDeathAnimation());
                flag = false;
            }
        }
    }
    private void FixedUpdate()
    {
        fuel -= valueOfDecreaseLevel;
        if (time >= interval)
        {
            time = 0;
            valueOfDecreaseLevel += 0.000000009f;
            Debug.Log("dec Fuel");
        }   
        time++;
    }
}

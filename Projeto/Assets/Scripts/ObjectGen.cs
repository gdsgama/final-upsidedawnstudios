﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGen : MonoBehaviour
{

    //O que este código faz chama-se object pooling
    //basicamente cria um conjunto de (poolSize) objetos iguais, e vai gerando copias a certo ponto e destruindo-as noutro

    class PoolObject //cada objeto desse conjunto vai ser uma instancia desta classe
    {
        public Transform transform; //vai ter uma posicione
        public bool inUse; // boolean que verifica se o objeto está em uso (no ecra), ou nao (pode ser instanciado para o ecrã)
        public PoolObject(Transform t) { transform = t; } // metodo que faz com que o transform do objeto seja o enviado pelo parametro
                                                          //setters para inUse
        public void Use() { inUse = true; }
        public void Dispose() { inUse = false; }
    }

    //basicamente um objeto com dois valores, o máximo em que os objetos podem spawnar e o mínimo 
    [System.Serializable]
    public struct YSpawnRange
    {
        public float minY;
        public float maxY;
    }

    public GameObject[] Prefab; // o conjunto de obstaculos a serem gerados
    public float shiftSpeed; //velocidade com que os objetos se aproximam
    public float spawnRate; //frequencia com que os objetos dao spawn

    public float minScale; //escala min do objeto
    public float maxScale; //escala max do objeto

    public float minYSpeed;
    public float maxYSpeed;

    public YSpawnRange ySpawnRange; // para definirmos o maximo e minimo dos valores de y em que se vai dar spawn
    public Vector3 defaultSpawnPos; // posiçao em que se vai dar spawn aos objetos, a direita da camera
    public Vector3 destroy;
    public float tempoAtivo;
    public Vector2 targetAspectRatio; //aspectRatio
    float spawnTimer; // timer para dar spawn
    PoolObject[] poolObjects; //conjunto de objetos da pool
    float targetAspect;

    public float timePassed = 0;
    public bool enabl;

    private int objCounter;

    void Start()
    {
        Configure(); // no inicio da scene faz certas configuraçoes
    }

    // método que aumenta a velocidade com que os obstaculos se movem, e a velocidade com que eles spawnam, para compensar
    // TODO: encontrar valores perfeitos
    void increaseShiftSpeedLimitSpawn()
    {
        if (shiftSpeed > -40 && spawnRate > 0.2f)
        {
            shiftSpeed -= 0.0013f;
            spawnRate -= 0.00010f;
        }
    }

    void Configure()
    {
        //spawning pool objects
        objCounter = 0;
        targetAspect = targetAspectRatio.x / targetAspectRatio.y;
        poolObjects = new PoolObject[Prefab.Length]; //define o vetor com os objetos
        for (int i = 0; i < poolObjects.Length; i++) // para cada indice do vetor, por no vetor uma copia do objeto
        {
            GameObject go = Instantiate(Prefab[i]) as GameObject;
            Transform t = go.transform;
            t.position = Vector3.one * 1000;
            poolObjects[i] = new PoolObject(t);
        }
    }


    void Update()
    {
        if (pausaMenu.gameIsPaused==0)
        {
            Shift(); //metodo que move objetos para a esquerda
            if (enabl)
            {
                spawnTimer += Time.deltaTime;
                if (spawnTimer > spawnRate)
                {
                    Spawn();
                    spawnTimer = 0;
                }
                timePassed += 0.1f;
                increaseShiftSpeedLimitSpawn();
            }
        }
    }

    void Spawn()
    {
        //moving pool objects into place
        PoolObject p = GetPoolObject();
        if (p == null) return;
        Vector3 pos = Vector3.zero;
        pos.y = Random.Range(ySpawnRange.minY, ySpawnRange.maxY);
        pos.x = (defaultSpawnPos.x * Camera.main.aspect) / targetAspect;
        p.transform.position = pos;
        float randomScale = Random.Range(minScale, maxScale);
        p.transform.localScale *= randomScale;
        Rigidbody2D rigidbody = p.transform.GetComponent<Rigidbody2D>();
        rigidbody.velocity = new Vector3(0, Random.Range(minYSpeed, maxYSpeed), 0);
        StartCoroutine(despawnWithTime(p, tempoAtivo));

    }

    void Shift()
    {
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].transform.position += Vector3.right * shiftSpeed * Time.deltaTime;
        }
    }


    PoolObject GetPoolObject()
    {
        if (objCounter == poolObjects.Length)
        {
            objCounter = 0;
        }
        if (!poolObjects[objCounter].inUse)
        {
            poolObjects[objCounter].Use();
            return poolObjects[objCounter];
        }
        objCounter++;

        return null;
    }
    IEnumerator despawnWithTime(PoolObject obj,float sec)
    {
        yield return new WaitForSeconds(sec);
        obj.Dispose();
        obj.transform.position = Vector3.right * 1000;
    }
}

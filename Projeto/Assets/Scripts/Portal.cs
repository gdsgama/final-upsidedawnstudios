﻿using System.Collections;
using UnityEngine;

public class Portal : MonoBehaviour
{
     public AudioClip portalClip;
     public AudioClip portalInitClip;
    Fuel fuelScript;
     Animator anim;
    GameObject player;
    AudioSource source;
    private void Start()
    {
        source = GetComponent<AudioSource>();
        source.volume = PlayerPrefs.GetFloat("fxVolume");
        fuelScript = GameObject.Find("Fuel").GetComponent<Fuel>();
        anim = GetComponent<Animator>();
        player = GameObject.Find("player");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        anim.SetTrigger("enter");
        StartCoroutine(portalActive());
    }
    IEnumerator portalActive()
    {
        //enables godPlayer
        player.layer = 17;
        source.PlayOneShot(portalInitClip);
        yield return new WaitForSeconds(.5f);
        if (PlayerPrefs.HasKey("vibrate"))Vibrator.Vibrate(30);
        source.PlayOneShot(portalClip);
        fuelScript.enabled=false;
        Time.timeScale = 5f;
        yield return new WaitForSeconds(5f);
        Time.timeScale = PlayerMovement.timeScaleSpeed;
        yield return new WaitForSeconds(1f);
        fuelScript.enabled=true;
        //disables godPlayer
        player.layer = 10;
    }
}

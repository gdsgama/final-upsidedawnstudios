﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PurchasesMenu : MonoBehaviour
{
    public GameObject popUPCanvas;
    public TypeWriterCommon writer;
    public Text popUpText;
    public Text coinsText;
    [Header("Audio")]
    public AudioSource source;
    public AudioClip noClip;
    public AudioClip coinClip;
    private void Start()
    {
        coinsText.text = PlayerPrefs.GetInt("coins").ToString();
    }
    public void clickBuy1()
    {
        //bought 500 pack
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 500);
        StartCoroutine(earnAnimation(500));


    }
    public void clickBuy2()
    {
        //bought 1500 pack
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 1500);
        StartCoroutine(earnAnimation(1500));
    }
    public void clickBuy3()
    {
        //bought 4000 pack
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + 4000);
        StartCoroutine(earnAnimation(4000));
    }
    public void clickExit()
    {
        SceneManager.LoadScene("UpgradeShop");
    }

    public void failedPurchase()
    {
        source.PlayOneShot(noClip);
        popUPCanvas.SetActive(true);
        StartCoroutine(writer.Type("The purchase was unsuccessful!"));
        StartCoroutine(popUpTimer());
    }


    IEnumerator earnAnimation(int x)
    {
        for (int i = 0; i < x / 100; i++)
        {
            source.PlayOneShot(coinClip);
            coinsText.text = (int.Parse(coinsText.text) + 100).ToString();
            yield return new WaitForSeconds(.045f);
        }
    }
    IEnumerator popUpTimer()
    {
        yield return new WaitForSeconds(3f);
        popUpText.text = "";
        popUPCanvas.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpgradeMenuConfirmAction : MonoBehaviour
{
    public string lastActioObjName;
    public int actionCost;
    public UpgradeMenuCoins coinsScript;
    public Animator anim;
    public AudioSource source;
    public AudioClip noClip;

    public void confirmBuy()
    {
        if (PlayerPrefs.GetInt("coins") >= actionCost)
        {
            var obj = GameObject.Find(lastActioObjName);
            //has money
            if (lastActioObjName.Equals("BatteryButton") || lastActioObjName.Equals("AgilityButton") || lastActioObjName.Equals("SpeedButton"))
            {
                //se for stat upgrade
                obj.GetComponent<UpgradePlusButton>().levelUp();
            }
            else
            {
                //se for habilidade
                obj.GetComponent<UpgradeMenuAbility>().buyAbility();
            }
            anim.SetTrigger("click");
            coinsScript.spendX(actionCost);
            //sound money aprove
            if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
            gameObject.SetActive(false);

        }
        else
        {
            //doesnt have money
            StartCoroutine(noVibration());
            source.PlayOneShot(noClip);
        }
    }

        IEnumerator noVibration()
        {
            if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(4);
            yield return new WaitForSeconds(.1f);
            if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LatestScore : MonoBehaviour
{

    Text scoreText;
    public int delay;
    string[] textes=new string[1];
    public TypeWriterCommon type;
    void Awake()
    {
        scoreText = GetComponent<Text>();
        textes[0] = "Score:" + PlayerPrefs.GetInt("LatestScore", 0).ToString();
        type.textsToType = textes;
    }
}

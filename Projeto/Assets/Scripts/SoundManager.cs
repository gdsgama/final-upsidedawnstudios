﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
    // Commented slider value (if adde in Main Game sliders PlayerPrefs code has to be changed also)
{
    public AudioSource musicSource;
    public AudioSource playerSource;


    //public Slider musicSlider;
    //public Slider fxSlider;


    void Start()
    {
        musicSource.volume = PlayerPrefs.GetFloat("musicVolume");
        playerSource.volume = PlayerPrefs.GetFloat("fxVolume");
        //RefreshVolumes();
    }

    //void ClosePause()
    //{
    //    PlayerPrefs.SetFloat("musicVolume", musicSlider.value);
    //    PlayerPrefs.SetFloat("fxVolume", fxSlider.value);
    //    RefreshVolumes();
    //}

    //void RefreshVolumes()
    //{
    //    musicSource.volume = musicSlider.value;
    //    playerSource.volume = fxSlider.value;

    //}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Warning : MonoBehaviour
{
    public GameObject warningObject;
    public GameObject laser;
    public int xPos;
    public AudioSource source;
    public AudioClip warninClip;
    public AudioClip wooshClip;

    private Animator anim;
    private GameObject obj;
    private float time;
    private bool flag;
    private float diference;

    void Start()
    {
        flag = true;
        obj = Instantiate(warningObject, new Vector3(xPos, laser.transform.position.y, 0), Quaternion.identity);
        anim = obj.GetComponent<Animator>();
        source.volume = PlayerPrefs.GetFloat("fxVolume");
    }

    private void FixedUpdate()
    {
        diference = this.transform.position.x - xPos;
        if (!(diference<200 && diference>30))
        {

            anim.SetTrigger("off");
            if (diference < 30  && diference>0)
            {
                source.PlayOneShot(wooshClip);
            }
            flag = true;

        }
        else
        {
            if (flag)
            {
                source.PlayOneShot(warninClip);
                anim.SetTrigger("on");
                flag = false;
            }
            time++;
        }
            obj.transform.position = new Vector3(xPos, laser.transform.position.y, 0);
    }
}
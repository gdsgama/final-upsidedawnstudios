﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ControlVolume : MonoBehaviour {

    public Slider slider;

    void Awake()
    {


        if (slider) {

            slider.value = AudioListener.volume;
            AudioListener.volume = PlayerPrefs.GetFloat("currentVol");
        }
        
    
    }
    public void VolumeControl (float volumeControl)
    {
        AudioListener.volume = volumeControl;
        PlayerPrefs.SetFloat(("currentVol"), AudioListener.volume);
    }


}

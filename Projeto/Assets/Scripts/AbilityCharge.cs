﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityCharge : MonoBehaviour
{
    public ParticleSystem particles;
    public int numberOfParticles;
    public int intervaloTempo;
    public float increaseInFuel;
    private int count;
    private void Start()
    {
        count = 0;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (Fuel.fuel < 1) StartCoroutine(giveFuel());
            if (count > intervaloTempo)
            {
                particles.Emit(numberOfParticles);
                count = 0;
            }
            else
            {
                count++;
            }

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            particles.Stop();
        }   
    }

     IEnumerator giveFuel()
    {
        yield return new WaitForSeconds(0.2f); 
        Fuel.fuel += increaseInFuel;
    }

}

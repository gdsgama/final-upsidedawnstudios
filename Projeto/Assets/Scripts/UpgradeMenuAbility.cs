﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenuAbility : MonoBehaviour
{
    public static string shieldDescription = "Activates a temporary shield that deflects any obstacle at the cost of 20% battery";
    public static string magnetDescription = "Activates a permanent magnet that attracts any obstacle nearby";
    public static int shieldCost = 2000;
    public static int magnetCost = 1000;
    public GameObject confirmBttn;

    Button bttn;
    Text description;
    Text cost;

    void Start()
    {
        description = GameObject.Find("Description").GetComponent<Text>();
        cost = GameObject.Find("Cost").GetComponent<Text>();
        bttn = GetComponent<Button>();
        if (gameObject.name.Equals("ShieldButton"))
        {
            if (PlayerPrefs.HasKey("shield"))
            {
                bttn.interactable = false;
            }
        }
        else
        {
            if (PlayerPrefs.HasKey("magnet"))
            {
                bttn.interactable = false;
            }
        }
    }
    public void preAbility()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        UpgradeMenuConfirmAction script = confirmBttn.GetComponent<UpgradeMenuConfirmAction>();
        script.lastActioObjName = gameObject.name;
        confirmBttn.SetActive(true);

        if (gameObject.name.Equals("ShieldButton"))
        {
            Debug.Log("shiield");
            script.actionCost = shieldCost;
            description.text = shieldDescription;
            cost.GetComponent<Text>().text = "Cost------" + shieldCost;
        }
        else
        {
            script.actionCost = magnetCost;
            description.text = magnetDescription;
            cost.GetComponent<Text>().text = "Cost------" + magnetCost;
        }
    }
    public void buyAbility()
    {
        if (gameObject.name.Equals("ShieldButton"))
        {
            PlayerPrefs.SetInt("shield", 1);
        }
        else
        {
            PlayerPrefs.SetInt("magnet", 1);
        }
        bttn.interactable = false;
        description.text = "";
        cost.text = "";
    }
}

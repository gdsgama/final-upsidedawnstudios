﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode()]
public class RectTransformLockPosition : MonoBehaviour
{
    private RectTransform rectTransform;
    public Vector3 startPosition;

    protected void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        //startPosition = transform.position;
    }

    private void LateUpdate()
    {
    
        {
            rectTransform.position = startPosition;
        }
    }
}
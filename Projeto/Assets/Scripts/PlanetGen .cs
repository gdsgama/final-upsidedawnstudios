﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGen : MonoBehaviour {

    //O que este código faz chama-se object pooling
    //basicamente cria um conjunto de (poolSize) objetos iguais, e vai gerando copias a certo ponto e destruindo-as noutro

    class PoolObject //cada objeto desse conjunto vai ser uma instancia desta classe
    {
        public Transform transform; //vai ter uma posicione
        public bool inUse; // boolean que verifica se o objeto está em uso (no ecra), ou nao (pode ser instanciado para o ecrã)
        public PoolObject(Transform t) { transform = t; } // metodo que faz com que o transform do objeto seja o enviado pelo parametro
        //setters para inUse
        public void Use() { inUse = true; } 
        public void Dispose() { inUse = false; }
    }

    //basicamente um objeto com dois valores, o máximo em que os objetos podem spawnar e o mínimo 
    [System.Serializable]
    public struct YSpawnRange
    {
        public float minY;
        public float maxY;
    }

    public GameObject[] Prefab; // o conjunto de obstaculos a serem gerados
    public int poolSize; // quantidade de objetos na "pool"
    public float shiftSpeed; //velocidade com que os objetos se aproximam
    public float spawnRate; //frequencia com que os objetos dao spawn

    public YSpawnRange ySpawnRange; // para definirmos o maximo e minimo dos valores de y em que se vai dar spawn
    public Vector3 defaultSpawnPos; // posiçao em que se vai dar spawn aos objetos, a direita da camera
    public Vector3 destroy;
    public Vector2 targetAspectRatio; //aspectRatio
    float spawnTimer; // timer para dar spawn
    PoolObject[] poolObjects; //conjunto de objetos da pool
    float targetAspect;

    public float timePassed = 0;
    public float intervaloDeTempoEmQueScoreAumenta;

    void Awake()
    {
        Configure(); // no inicio da scene faz certas configuraçoes
        InvokeRepeating("increaseShiftSpeed", 5, 2); //desde o inicio do jogo, a velocidade com que os obstaculos se movem vai aumentando
    }

    // método que aumenta a velocidade com que os obstaculos se movem, e a velocidade com que eles spawnam, para compensar
    // TODO: encontrar valores perfeitos
    void increaseShiftSpeed() {
        shiftSpeed -= 0.05f;
        spawnRate -= 0.03f;
    }

    void Configure()
    {
        //spawning pool objects
        targetAspect = targetAspectRatio.x / targetAspectRatio.y;
        poolObjects = new PoolObject[poolSize]; //define o vetor com os objetos
        for (int i = 0; i < poolObjects.Length; i++) // para cada indice do vetor, por no vetor uma copia do objeto
        {
            GameObject go = Instantiate(Prefab[i]) as GameObject;
            Transform t = go.transform;
            t.SetParent(transform);
            t.position = Vector3.one * 1000;
            poolObjects[i] = new PoolObject(t);
        }
    }


    void Update()
    {
        Shift(); //metodo que move objetos para a esquerda
        spawnTimer += Time.deltaTime;
        if (spawnTimer > spawnRate)
        {
            Spawn();
            spawnTimer = 0;
        }
        timePassed += 0.1f;
        if (timePassed >= intervaloDeTempoEmQueScoreAumenta)
        {
            shiftSpeed = shiftSpeed - 0.001f;
            timePassed = 0;
        }
    }

    void Spawn()
    {
        //moving pool objects into place
        Transform t = GetPoolObject();
        if (t == null) return;
        Vector3 pos = Vector3.zero;
        pos.y = Random.Range(ySpawnRange.minY, ySpawnRange.maxY);
        pos.x = (defaultSpawnPos.x * Camera.main.aspect) / targetAspect;
        t.position = pos;
    }

    void Shift()
    {
        //loop through pool objects 
        //moving them
        //discarding them as they go off screen
        for (int i = 0; i < poolObjects.Length; i++)
        {
            poolObjects[i].transform.position += Vector3.right * shiftSpeed * Time.deltaTime;
            CheckDisposeObject(poolObjects[i]);
        }
    }

    void CheckDisposeObject(PoolObject poolObject)
    {
        //place objects off screen
        if (poolObject.transform.position.x < (destroy.x * Camera.main.aspect) / targetAspect)
        {
            poolObject.Dispose();
            poolObject.transform.position = Vector3.one * 1000;
        }
    }

    Transform GetPoolObject()
    {
        //retrieving a random pool object
          int i=  Random.Range(0,poolObjects.Length - 1);
            if (!poolObjects[i].inUse)
            {
                poolObjects[i].Use();
                return poolObjects[i].transform;
            }
       
        return null;
    }
}

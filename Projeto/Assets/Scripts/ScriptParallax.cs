﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptParallax : MonoBehaviour
{

    // Background scroll speed can be set in Inspector with slider
    [Range(0.2f, 5f)]
    public float scrollSpeed;

    // Scroll offset value to smoothly repeat backgrounds movement
    public float scrollOffset;

    // Start position of background movement
    Vector2 startPos;

    // Backgrounds new position
    float newPos;

    public static float timePassed = 100;
    public static float intervaloDeTempoEmQueScoreAumenta = 0.1f;

    // Use this for initialization
    void Start()
    {
        // Getting backgrounds start position
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        // Calculating new backgrounds position repeating it depending on scrollOffset
        //if (!pausaMenu.GameIsPaused)
        //{   

            newPos = Mathf.Repeat(Time.time * -scrollSpeed, scrollOffset);
            // Setting new position
            transform.position = startPos + Vector2.right * newPos;

            timePassed += 0.1f;
            if (timePassed >= intervaloDeTempoEmQueScoreAumenta)
            {
                scrollSpeed += 0.0003f;
                timePassed = 0;
            }
        //}
    }
}

﻿using System.Collections;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class GoogleAdController : MonoBehaviour
{

    public static string PLAY_ID = "ca-app-pub-4685062442663337~6492035787";
    public static string STATIC_ID = "ca-app-pub-4685062442663337/1285324023";
    public static string REWARDED_ID = "ca-app-pub-4685062442663337/8537607013";
    public static string TEST_STATIC_ID = "ca-app-pub-3940256099942544/1033173712";

    public EndGameManager endGameManager;
    public static bool display = true;
    public static uint menusClicks = 0;

    static InterstitialAd staticAd;
    static RewardBasedVideoAd rewardAd;


    public static void initAd()
    {
        MobileAds.Initialize(initStatus => { });
    }

    public static void loadStaticAd()
    {
        if (staticAd==null || !staticAd.IsLoaded())
        {
            staticAd = new InterstitialAd(STATIC_ID);
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            staticAd.LoadAd(request);
        }
    }

    public static void loadRewardedAd()
    {
        if (rewardAd==null || !rewardAd.IsLoaded())
        {
            rewardAd = RewardBasedVideoAd.Instance;
            // Load the rewarded video ad with the request
            AdRequest request = new AdRequest.Builder().Build();
            rewardAd.LoadAd(request, REWARDED_ID);
        }
    }

    public void displayStaticAd()
    {
        if (MainMenu.proVersion) return;
        if (display || menusClicks>= 3)
        {
            loadStaticAd();
            staticAd.OnAdClosed += HandleOnAdClosed;
            StartCoroutine(loadStaticWhenReady());
            display = false;
            menusClicks = 0;
        }
    }

    public void displayRewardAd()
    {
        if (MainMenu.proVersion) return;
        loadRewardedAd();
        // Called when an ad request failed to load.
        rewardAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when the user should be rewarded for watching a video.
        rewardAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardAd.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardAd.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
        StartCoroutine(loadStaticRewardWhenReady());
    }

    IEnumerator loadStaticWhenReady()
    {
        do
        {
            Debug.Log("static ad not ready");
            yield return new WaitForSeconds(.1f);
        } while (!staticAd.IsLoaded());
        staticAd.Show();
        // shows the interstitial
    }
    IEnumerator loadStaticRewardWhenReady()
    {
        do
        {
            Debug.Log("reward ad not ready");
            yield return new WaitForSeconds(.1f);
        } while (!rewardAd.IsLoaded());
        rewardAd.Show();
        // Load the interstitial with the request.
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
        endGameManager.noClick();
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        endGameManager.noClick();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);
        endGameManager.yesClick();
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
        endGameManager.noClick();
    }
    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
        staticAd.Destroy();
    }
}

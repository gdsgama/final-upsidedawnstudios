﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeWriter : MonoBehaviour
{
    public string[] textsToType;
    public float delay;
    public float delayDeleting;
    public int delayBetweenPhrases;
    public float ping;
    public Text currentText;
    public AudioSource source;
    public AudioClip typeClip;
    public bool instaDelete;

    void delete()
    {
        StartCoroutine(Delete());
    }
    public IEnumerator TypeAll()
    {
        for (int i = 0; i < textsToType.Length; i++)
        {
            Coroutine typing = StartCoroutine(Type(textsToType[i]));
            yield return typing;
            Coroutine iddle = StartCoroutine(IddleText());
            yield return new WaitForSeconds(delayBetweenPhrases);
            StopCoroutine(iddle);
            if (!instaDelete)
            {
                Coroutine deleting = StartCoroutine(Delete());
                yield return deleting;
            }
            currentText.text = "";
        }
    }

    IEnumerator Type(string text)
    {
        yield return new WaitForSeconds(0.4f);
        for (int i = 0; i <= text.Length; i++)
        {
            source.PlayOneShot(typeClip);
            currentText.text = text.Substring(0, i) + '|';
            yield return new WaitForSeconds(delay);
        }
    }
    IEnumerator Delete()
    {
        int end = currentText.text.Length;
        for (int i = end; i >= 0; i--)
        {
            if (i == 0)
            {
                currentText.text = "";
            }
            else
            {
                currentText.text = currentText.text.Remove(i - 1) + '|';
            }
            yield return new WaitForSeconds(delayDeleting);
        }
    }
    IEnumerator IddleText()
    {
        for (int i = 0; i < 90 * 90; i++)
        {
            currentText.text = currentText.text.Remove(currentText.text.Length - 1);
            yield return new WaitForSeconds(ping);
            source.PlayOneShot(typeClip);
            currentText.text = (currentText.text + '|');
            yield return new WaitForSeconds(ping);
        }

    }
}

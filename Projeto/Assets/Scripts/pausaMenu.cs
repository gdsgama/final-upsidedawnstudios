﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class pausaMenu : MonoBehaviour
{
    public static float timeStored;
    public static int gameIsPaused;




    public float delayToPauseTime;
    public Animator screenAnim;
    public GameObject textObj;
    public AudioLowPassFilter lpFilter;

    [Header("Pause Menu Text Content")]
    public Text highscoreText;
    public Text coinsText;
    public Text PlanetsText;

    private Animator textAnim;

    private void Start()
    {
        textObj.SetActive(false);
        textAnim = textObj.GetComponent<Animator>();
        gameIsPaused = 0;
        PlanetsText.text = "Planets discovered: "+planetsDiscovered() + "/8";
    }
    private void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused == 0)
            {
                StartCoroutine(Pause());

            }
            else if (gameIsPaused == 1)
            {
                StartCoroutine(Resume());
            }
        }
    }
    public IEnumerator Resume()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(11);
        gameIsPaused = -1;
        Time.timeScale = PlayerMovement.timeScaleSpeed;
        textAnim.SetTrigger("disappear");
        lpFilter.cutoffFrequency = 10000;
        yield return new WaitForSeconds(0.8f);
        screenAnim.SetTrigger("Resume");
        ContadorScore.tempoCorre = true;
        yield return new WaitForSeconds(delayToPauseTime);
        textObj.SetActive(false);
        gameIsPaused = 0;
    }

    public IEnumerator Pause()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(11);
        coinsText.text = "Coins: " + PlayerPrefs.GetInt("coins");
        highscoreText.text = "Highscore: " + PlayerPrefs.GetInt("Highscore");
        textObj.SetActive(true);
        gameIsPaused = -1;
        screenAnim.SetTrigger("Pause");
        lpFilter.cutoffFrequency = 1100;
        ContadorScore.tempoCorre = false;
        gameIsPaused = 1;
        yield return new WaitForSeconds(1);
        textObj.SetActive(true);
        textAnim.SetTrigger("appear");
        yield return new WaitForSeconds(2f);
        Time.timeScale = 0f;
        timeStored = Time.time;

    }


    public void goToMenuScene()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(11);
        ContadorScore.tempoCorre = true;
        ContadorScore.resetScore();
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
        // assim ele nao grava o score atual. se quiserem que grave temos de por aqui a funçao
    }
    public static int planetsDiscovered()
    {
        for (int i = 8; i > 0; i--)
        {
            if (PlayerPrefs.HasKey("planet" + i))
                return i;
        }
        return 0;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBetweenObstacles : MonoBehaviour
{
    public ObjectGen rockScript;
    public ObjectGen sceneRockScript1;
    public ObjectGen sceneRockScript2;
    public float rockDurSeconds;
    public SpriteRenderer warningSource;
    public Animator warningController;
    public Sprite upDownSprite;
    public Sprite safePlaceWarningSprite;
    public Sprite randomMeteorSprite;
    public GameObject upDownMeteor;
    public GameObject randomMeteor;
    public GameObject safeMeteor;
    public AudioSource source;
    public AudioClip nebulaClip;


    private int version;
    private int count;
    private bool flag;

    private static int time;

    private void Start()
    {
        time = 0;
        version = 0;
        count = 0;
        flag = false;
        rockDurSeconds /= Time.fixedDeltaTime;
    }
    private void FixedUpdate()
    {
        if (pausaMenu.gameIsPaused != 1)
        {

            if (rockScript.enabl)                                                   //default rock part      
            {
                if (time > rockDurSeconds)
                {
                    rockScript.enabl = false;
                    sceneRockScript1.enabl = false;
                    sceneRockScript2.enabl = false;
                    flag = true;
                }
                else
                {
                    //Debug.Log("increasing time");
                    time++;
                }
            }
            else if (flag)
            {
                if (version > 2)
                {
                    version = 0;
                }
                flag = false;
                switch (version)
                {
                    case 0:
                        StartCoroutine(NormalMeteorSpawn());
                        break;
                    case 1:
                        StartCoroutine(UpAndDownMeteorSpawn());
                        break;
                    case 2:
                        StartCoroutine(MultipleSafePlaceMeteorSpawn());
                        break;
                }
            }
        }
    }
    IEnumerator NormalMeteorSpawn()
    {
        yield return new WaitForSeconds(4);
        warningSource.sprite = randomMeteorSprite;
        warningController.SetTrigger("fadeIn");
        yield return new WaitForSeconds(4);
        warningController.SetTrigger("fadeOut");
        rockScript.enabled = false;
        sceneRockScript1.enabled = false;
        sceneRockScript2.enabled = false;

        ObjectGen normalMeteor = gameObject.AddComponent<ObjectGen>();
        randomMeteorPresetCoded(normalMeteor);
        yield return new WaitForSeconds(8 + 2 * count);

        normalMeteor.enabl = false;
        yield return new WaitForSeconds(2);

        Destroy(normalMeteor);
        time = 0;
        //increaseNormalSpeed();
        version++;
        rockScript.enabled = true;
        rockScript.enabl = true;
        sceneRockScript1.enabled = true;
        sceneRockScript1.enabl = true;
        sceneRockScript2.enabled = true;
        sceneRockScript2.enabl = true;
    }

    IEnumerator UpAndDownMeteorSpawn()
    {
        yield return new WaitForSeconds(4);
        warningSource.sprite = upDownSprite;
        warningController.SetTrigger("fadeIn");
        yield return new WaitForSeconds(4);
        warningController.SetTrigger("fadeOut");
        rockScript.enabled = false;
        sceneRockScript1.enabled = false;
        sceneRockScript2.enabled = false;

        ObjectWave waveMeteor = gameObject.AddComponent<ObjectWave>();
        upAndDownPresetCoded(waveMeteor);
        waveMeteor.numOberOfWaves = (2 + 2 * count);
        yield return new WaitForSeconds(waveMeteor.tempoAtivo * waveMeteor.numOberOfWaves * 3.5f);


        Destroy(waveMeteor);

        time = 0;
        increaseNormalSpeed();
        version++;
        rockScript.enabled = true;
        rockScript.enabl = true;
        sceneRockScript1.enabled = true;
        sceneRockScript1.enabl = true;
        sceneRockScript2.enabled = true;
        sceneRockScript2.enabl = true;
    }
    IEnumerator SingleSafePlaceMeteorSpawn()
    {
        float randomHeight = Random.Range(-8, 8);
        Debug.Log("random height: " + randomHeight);
        warningSource.sprite = safePlaceWarningSprite;
        warningSource.gameObject.transform.SetPositionAndRotation(new Vector3(8, randomHeight), Quaternion.identity);

        warningController.SetTrigger("fadeIn");
        yield return new WaitForSeconds(2.3f);
        warningController.SetTrigger("fadeOut");
        source.PlayOneShot(nebulaClip);
        ObjectWave waveMeteor = gameObject.AddComponent<ObjectWave>();
        safePlacePresetCoded(waveMeteor);
        waveMeteor.minY = randomHeight + 3;
        waveMeteor.maxY = randomHeight - 3;
        yield return new WaitForSeconds(waveMeteor.tempoAtivo + 2);
        Destroy(waveMeteor);
    }
    IEnumerator MultipleSafePlaceMeteorSpawn()
    {
        yield return new WaitForSeconds(8);
        rockScript.enabled = false;
        sceneRockScript1.enabled = false;
        sceneRockScript2.enabled = false;
        warningSource.gameObject.transform.localScale -= new Vector3(1.2f, 1.2f, 1);
        for (int i = 0; i <= 2 * count; i++)
        {
            StartCoroutine(SingleSafePlaceMeteorSpawn());
            yield return new WaitForSeconds(5);
        }
        warningSource.gameObject.transform.localScale += new Vector3(1.2f, 1.2f, 1);
        warningSource.gameObject.transform.SetPositionAndRotation(new Vector3(0, 0), Quaternion.identity);
        time = 0;
        increaseNormalSpeed();
        version++;
        rockScript.enabled = true;
        rockScript.enabl = true;
        sceneRockScript1.enabled = true;
        sceneRockScript1.enabl = true;
        sceneRockScript2.enabled = true;
        sceneRockScript2.enabl = true;
    }
    private void increaseNormalSpeed()
    {
        rockScript.shiftSpeed -= 0.025f;
        rockScript.spawnRate -= 0.0802f;
        sceneRockScript1.shiftSpeed -= 0.025f;
        sceneRockScript1.spawnRate -= 0.002f;
        sceneRockScript2.shiftSpeed -= 0.025f;
        sceneRockScript2.spawnRate -= 0.002f;
        count++;
    }
    private void upAndDownPresetCoded(ObjectWave wave)
    {
        wave.minY = -10;
        wave.maxY = 10;
        wave.objects = new GameObject[17];
        GameObject temp = upDownMeteor;
        temp.GetComponent<AudioSource>().volume = 0.78f;
        for (int i = 0; i < 17; i++)
        {
            wave.objects[i] = temp;
        }
        wave.delayBeetweenspawns = 0.1f;
        wave.delayBeetWeenWaves = 4;
        wave.xSpawnPosition = 100;
        wave.shiftSpeed = 200;
        wave.spaceBetweenMeteors = 1;
        wave.tempoAtivo = 1;
        wave.targetAspectRatio = new Vector2(16, 10);
    }
    private void safePlacePresetCoded(ObjectWave wave)
    {
        wave.objects = new GameObject[17];
        for (int i = 0; i < 17; i++)
        {
            wave.objects[i] = safeMeteor;
        }
        wave.delayBeetweenspawns = 1e-9f;
        wave.delayBeetWeenWaves = 1e-9f;
        wave.xSpawnPosition = 100;
        wave.shiftSpeed = 550;
        wave.numOberOfWaves = 2;
        wave.spaceBetweenMeteors = 1;
        wave.tempoAtivo = 2;
        wave.targetAspectRatio = new Vector2(16, 10);
    }
    private void randomMeteorPresetCoded(ObjectGen gen)
    {
        gen.Prefab = new GameObject[] { randomMeteor, randomMeteor };
        gen.spawnRate = 0.7f;
        gen.shiftSpeed = -220;
        gen.minScale = 1;
        gen.maxScale = 1;
        gen.minYSpeed = 0;
        gen.maxYSpeed = 0;
        gen.ySpawnRange.minY = -10;
        gen.ySpawnRange.maxY = 10;
        gen.defaultSpawnPos = new Vector3(190, 0, 0);
        gen.tempoAtivo = 2;
        gen.targetAspectRatio = new Vector2(16, 10);
        gen.timePassed = 0;
        gen.enabl = true;
    }
}

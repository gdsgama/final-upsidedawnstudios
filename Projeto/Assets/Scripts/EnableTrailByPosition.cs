﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTrailByPosition : MonoBehaviour
{
    public int enableLimitEnd;
    public int disbaleLimitStart;

    private TrailRenderer trail;
    private float currentPos;

    private void Start()
    {
        trail = this.GetComponent<TrailRenderer>();
    }
    void Update()
    {
        currentPos = this.transform.position.x;
        if (currentPos > enableLimitEnd)
        {
            trail.enabled = true;
        }
        else if (currentPos < disbaleLimitStart)
        {
            trail.enabled = false;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(waitForVideo());
    }
    IEnumerator waitForVideo()
    {
        Application.targetFrameRate = 60;
       yield return new WaitForSeconds(6.5f);
        Vibrator.Vibrate(80);
       yield return new WaitForSeconds(1.2f);
        SceneManager.LoadSceneAsync("Menu");
    }
}

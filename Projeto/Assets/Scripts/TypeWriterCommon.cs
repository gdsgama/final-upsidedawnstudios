﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TypeWriterCommon : MonoBehaviour
{
    [Multiline]
    public string[] textsToType;
    public float delay;
    public float delayDeleting;
    public float delayBetweenPhrases;
    public float ping;
    public Text currentText;
    public float timeDelay;
    public AudioSource source;
    public AudioClip typeClip;
    public bool isTyping;
    void Start()
    {
        isTyping = false;
        if (this.enabled)
        {
            StartCoroutine(TypeAll());
        }
    }

    void delete()
    {
        StartCoroutine(Delete());
    }
    IEnumerator TypeAll()
    {
        yield return new WaitForSeconds(timeDelay);
        for (int i = 0; i < textsToType.Length; i++)
        {
            Coroutine typing = StartCoroutine(Type(textsToType[i]));
            yield return typing;
            yield return new WaitForSeconds(delayBetweenPhrases);
            if (i != textsToType.Length - 1)
            {
                Coroutine deleting = StartCoroutine(Delete());
                yield return deleting;
            }
        }
    }

    public IEnumerator Type(string text)
    {
        isTyping = true;
        for (int i = 0; i <= text.Length; i++)
        {
            if (i == text.Length - 1)
            {
                source.PlayOneShot(typeClip);
                currentText.text = text.Substring(0, i) + '|';
            }
            else
            {
                source.PlayOneShot(typeClip);
                currentText.text = text.Substring(0, i);
            }
            yield return new WaitForSeconds(delay);
        }
        isTyping = false;
    }
   public  IEnumerator Delete()
    {
        int end = currentText.text.Length;
        for (int i = end; i >= 0; i--)
        {
            if (i == 0)
            {
                currentText.text = "";
            }
            else
            {
                currentText.text = currentText.text.Remove(i - 1) + '|';
            }
            yield return new WaitForSeconds(delayDeleting);
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorScore : MonoBehaviour {

    static int scoreValor = 0;
    Text scoreTexto;
    public static bool tempoCorre = true;
    public float timePassed = 0;
    public float intervaloDeTempoEmQueScoreAumenta;

	void Start () {
        scoreTexto = GetComponent<Text>();
	}
	
	void FixedUpdate () { 
        if (tempoCorre)
        {
            timePassed += 0.1f;
            if (timePassed >= intervaloDeTempoEmQueScoreAumenta)
            {
                scoreValor++;
                scoreTexto.text = scoreValor.ToString();
                timePassed = 0;
            }
        }
	}

    public static int getScore() {
        return scoreValor;
    }

    public static void resetScore() {
        scoreValor = 0;
    }
}

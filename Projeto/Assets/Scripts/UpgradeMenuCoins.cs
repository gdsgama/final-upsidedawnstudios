﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UpgradeMenuCoins : MonoBehaviour
{
    public AudioSource source;
    public AudioClip coinClip;
    Text coinsText;

    void Start() 
    {
       coinsText = GetComponent<Text>();
        coinsText.text = PlayerPrefs.GetInt("coins").ToString();
    }

    public void spendX(int x)
    {
        PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") - x);
        StartCoroutine(spendAnimation(x));
    }
    IEnumerator spendAnimation(int x)
    {
        for(int i=0; i < x/100; i++)
        {
            coinsText.text = (int.Parse(coinsText.text) - 100).ToString();
            source.PlayOneShot(coinClip);
            yield return new WaitForSeconds(.045f);
        }
    }
    public void shop()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        SceneManager.LoadScene("PurchasesMenu");
    }

    //////////Relative to back Button
    public void backToMenu()
    {
        MainMenu.back =1;
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        SceneManager.LoadScene("Menu");
    }
   }

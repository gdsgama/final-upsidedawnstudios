﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour {

    Text scoreText;

    void Awake() {
        DontDestroyOnLoad(gameObject);
        scoreText = GetComponent<Text>();
    }

    private void Update()
    {
        scoreText.text = "Highscore: " + PlayerPrefs.GetInt("Highscore", 0).ToString();

    }
}

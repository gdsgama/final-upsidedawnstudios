﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSystem : MonoBehaviour
{
    public int rarity;
    public AudioClip coinSound;
    public SpriteRenderer rend;
    PlayerMovement playerMov;

    public static int baseVal = 5;

    AudioSource source;
    private void Start()
    {
        source = GameObject.Find("CoinPlayer").GetComponent<AudioSource>();
        playerMov = GameObject.Find("player").GetComponent<PlayerMovement>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            rend.enabled = false;
            PlayerPrefs.SetInt("coins", PlayerPrefs.GetInt("coins") + baseVal * rarity);
            playerMov.cointHit(transform.parent.gameObject);
            source.PlayOneShot(coinSound);
        }
        
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public GameObject[] planets;
    [Multiline]
    public string standardMessage;
    [Multiline]
    public string[] messagesDiscovered;
    public ObjectGen normalGen;
    public TypeWriterCommon planetTyper;
    public ContadorScore contScore;
    public Fuel fu;
    public GameObject speedEffect;
    public GameObject player;
    public Animator cameraAnim;
    public RotateBetweenObstacles scriptRotate;
    public ObjectGen scriptBolt;
    public ObjectGen scriptNormal;
    public Animator fadeAnimator;
    public GameObject finalTextObj;
    public GameObject magnetObj;
    public AudioSource source;
    SpriteRenderer playerSpriter;
    int score;

    bool flag;

    ObjectGen planetGen;

    private void Start()
    {
        playerSpriter = GameObject.Find("player").GetComponent<SpriteRenderer>();
        if (PlayerPrefs.HasKey("magnet")) magnetObj.SetActive(true);
        flag = true;
        if (PlayerPrefs.GetInt("boost") != 0)
        {
            StartCoroutine(boost(PlayerPrefs.GetInt("boost")));
        }
    }


    void Update()
    {
        score = ContadorScore.getScore();
        if (score % 800 == 0 && score != 0)
        {
            if (flag)
            {
                if (score == 800)
                {
                    StartCoroutine(PlanetGen(planets[0]));
                    if (PlayerPrefs.HasKey("planet1"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[0], 5, 8));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet1", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }
                }
                else if (score == 1600)
                {
                    StartCoroutine(PlanetGen(planets[1]));
                    if (PlayerPrefs.HasKey("planet2"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[1], 4.8f, 7.8f));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet2", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }

                }
                else if (score == 2400)
                {
                    StartCoroutine(PlanetGen(planets[2]));
                    if (PlayerPrefs.HasKey("planet3"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[2], 4.6f, 7.6f));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet3", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }
                }
                else if (score == 3200)
                {
                    StartCoroutine(PlanetGen(planets[3]));
                    if (PlayerPrefs.HasKey("planet4"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[2], 4.6f, 7.6f));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet4", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }
                }
                else if (score == 4000)
                {
                    StartCoroutine(PlanetGen(planets[4]));
                    if (PlayerPrefs.HasKey("planet5"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[2], 4.6f, 7.6f));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet5", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }
                }
                else if (score == 4800)
                {
                    StartCoroutine(PlanetGen(planets[5]));
                    if (PlayerPrefs.HasKey("planet6"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[2], 4.6f, 7.6f));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet6", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }
                }
                else if (score == 5600)
                {
                    StartCoroutine(PlanetGen(planets[6]));
                    if (PlayerPrefs.HasKey("planet7"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[2], 4.6f, 7.6f));
                    }
                    else
                    {
                        PlayerPrefs.SetInt("planet7", 1);
                        StartCoroutine(typeMessageAndDelete(standardMessage, 5, 8));
                    }
                }
                else if (score == 6400)
                {
                    StartCoroutine(PlanetGen(planets[7]));
                    if (PlayerPrefs.HasKey("planet8"))
                    {
                        StartCoroutine(typeMessageAndDelete(messagesDiscovered[2], 4.6f, 7.6f));
                        StartCoroutine(endCutScene());
                    }

                }
                flag = false;
            }
        }
    }
    IEnumerator PlanetGen(GameObject planet)
    {
        planetGen = gameObject.AddComponent<ObjectGen>();
        applyPlanetPresetCoded(planetGen);
        planetGen.Prefab = new GameObject[] { planet };
        yield return new WaitForSeconds(planetGen.tempoAtivo + 1);
        Destroy(planetGen);
        flag = true;
    }
    IEnumerator typeMessageAndDelete(string message, float firstDelay, float lastDelay)
    {
        yield return new WaitForSeconds(firstDelay);
        StartCoroutine(planetTyper.Type(message));
        yield return new WaitForSeconds(lastDelay);
        StartCoroutine(planetTyper.Delete());
    }
    private void applyPlanetPresetCoded(ObjectGen planetGen)
    {
        planetGen.shiftSpeed = 0.5f * normalGen.shiftSpeed;
        planetGen.spawnRate = 1;
        planetGen.minScale = 1;
        planetGen.maxScale = 1;
        planetGen.minYSpeed = 0;
        planetGen.maxYSpeed = 0;
        planetGen.ySpawnRange.minY = 0;
        planetGen.ySpawnRange.maxY = 0;
        planetGen.defaultSpawnPos = new Vector3(26, 0, 0);
        planetGen.tempoAtivo = 26;
        planetGen.targetAspectRatio = new Vector2(16, 10);
        planetGen.timePassed = 0;
        planetGen.enabl = true;
    }
    IEnumerator boost(float score)
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(100);
        scriptRotate.enabled = false;
        scriptBolt.enabled = false;
        flag = false;
        fu.enabled = false;
        speedEffect.SetActive(true);
        //enables godPlayer
        player.layer = 17;
        yield return new WaitForSeconds(1.1f);
        cameraAnim.SetTrigger("shake");
        Time.timeScale = 34;
        var trueVal = score * ((Time.fixedDeltaTime / contScore.intervaloDeTempoEmQueScoreAumenta) - 0.00475f);
        trueVal /= 12;
        yield return new WaitForSeconds(trueVal * Time.timeScale);

        speedEffect.SetActive(false);
        flag = true;
        fu.enabled = true;
        cameraAnim.SetTrigger("endShake");
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(40);
        Time.timeScale = PlayerMovement.timeScaleSpeed;
        scriptRotate.enabled = true;
        scriptBolt.enabled = true;
        StartCoroutine(blink());
        PlayerPrefs.SetInt("boost", 0);
        Debug.LogError("colocou a 0");
        yield return new WaitForSeconds(3.5f);
        //disables godPlayer
        player.layer = 10;
    }
    IEnumerator blink()
    {
        playerSpriter.enabled = false;
        yield return new WaitForSeconds(.05f);
        playerSpriter.enabled = true;
        yield return new WaitForSeconds(.05f);
        playerSpriter.enabled = false;
        yield return new WaitForSeconds(.05f);
        playerSpriter.enabled = true;
        yield return new WaitForSeconds(.05f);
        playerSpriter.enabled = false;
        yield return new WaitForSeconds(.05f);
        playerSpriter.enabled = true;
    }

    IEnumerator endCutScene()
    {
        source.volume = 0.25f;
        StartCoroutine(giveLastFuel());
        scriptBolt.enabl = false;
        scriptNormal.enabl = false;
        yield return new WaitForSeconds(8);
        scriptBolt.enabled = false;
        scriptRotate.enabled = false;
        scriptNormal.enabled = false;
        PlayerPrefs.SetInt("endingText", 1);
        yield return new WaitForSeconds(2);
        Debug.LogWarning("end anim");
        fadeAnimator.SetTrigger("end");
        yield return new WaitForSeconds(7);
        finalTextObj.SetActive(true);
    }
    IEnumerator giveLastFuel()
    {
        for (uint i = 0; i < 10; i++)
        {
            Fuel.fuel += 0.05f;
            yield return new WaitForSeconds(0.1f);
        }

    }

}

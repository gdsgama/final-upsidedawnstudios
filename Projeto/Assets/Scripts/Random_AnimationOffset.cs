﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Random_AnimationOffset : MonoBehaviour
{
    public Animator anim;
    void Awake()
    {
        AnimatorStateInfo state = anim.GetCurrentAnimatorStateInfo(0);
        anim.Play(state.fullPathHash, -1, Random.Range(0f, 1.4f));
    }
}
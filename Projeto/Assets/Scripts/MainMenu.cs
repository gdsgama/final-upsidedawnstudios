﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public static bool proVersion = true;
    public static uint back;

    public Animator screenAnim;
    public Animator firstTextAnim;
    public Text highScore;
    public AudioClip backgroundMusic;
    public TypeWriter initialTextScript;
    public Animator buttonsAnim;
    public Animator buttonsChildAnim;
    public GameObject consoleCanvas;
    public GameObject popUpCanvas;
    public Text hardcoreText;
    public Button hardcoreButton;
    public Image planet;
    public Sprite[] planets;


    private AudioSource source2 { get { return GetComponent<AudioSource>(); } }
    private AudioSource source { get { return GetComponent<AudioSource>(); } }
    private bool enter;
    private void Awake()
    {
        back = 0;
    }
    private void Start()
    {
        
        GoogleAdController.loadStaticAd();
        GoogleAdController.loadRewardedAd();
        enter = true;
        Debug.Log("Back "+back);
        if (back == 1)
        {
            firstTextAnim.gameObject.SetActive(false);
            consoleCanvas.SetActive(true);
            StartCoroutine(ScreenText());
            StartCoroutine(NormalRun());
            enter = false;
        }
        else
        {
            //screenAnim.gameObject.SetActive(true);
            back = 1;
        }
        GoogleAdController.initAd();
        planet.sprite = planets[pausaMenu.planetsDiscovered()];
        Time.timeScale = 1;
        highScore.text = PlayerPrefs.GetInt("Highscore", 0).ToString();
        gameObject.AddComponent<AudioSource>();
        gameObject.AddComponent<AudioSource>();
        source2.PlayOneShot(backgroundMusic);
        source2.volume = 0.69f;
        source.playOnAwake = false;
    }

    private void Update()
    {
        if (enter && Input.GetMouseButtonDown(0))
        {
            consoleCanvas.SetActive(true);
            firstTextAnim.SetTrigger("TClick");
            screenAnim.SetTrigger("Pause");
            if (!PlayerPrefs.HasKey("firstTime"))
            {
                PlayerPrefs.SetInt("firstTime", 1);
                StartCoroutine(ScreenText());
                StartCoroutine(FirstRun());
            }
            else
            {
                StartCoroutine(ScreenText());
                StartCoroutine(NormalRun());
            }
            enter = false;
        }
    }

    public void preLaunchClick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        consoleCanvas.SetActive(false);
        popUpCanvas.SetActive(true);
        if (PlayerPrefs.HasKey("planet8") || proVersion)
        {
            hardcoreButton.interactable = true;
            hardcoreText.color=new Color(255, 255, 255, 255);
        }
    }

    public void hardcoreClick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        SceneManager.LoadScene("LoadingHardcore");
    }

    public void startButton()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        StartCoroutine(ComecarJogo());
    }
    public void workShopButton()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        StartCoroutine(ToWorkshop());
    }

    public void optionsButton()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        StartCoroutine(ToOptions());
    }

    public void creditsButton()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        StartCoroutine(ToCredtis());
    }
    public void exploreButton()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        StartCoroutine(ToExplore());
    }
    public IEnumerator ComecarJogo()
    {
        screenAnim.SetTrigger("Launch");
        yield return new WaitForSeconds(0.3f);
        Time.timeScale = 1;
        SceneManager.LoadScene("Loading");
    }
    public IEnumerator ToExplore()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("Explore");
    }

    public IEnumerator ToCredtis()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("Credits");
    }
    public IEnumerator ToWorkshop()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("UpgradeShop");
    }
    public IEnumerator ToOptions()
    {
        yield return new WaitForSeconds(0.3f);
        SceneManager.LoadScene("Options");
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("mainGame"); // Para comecar o jogo
    }
    IEnumerator ScreenText()
    {
        yield return new WaitForSeconds(1);
        screenAnim.SetTrigger("In");

    }
    IEnumerator FirstRun()
    {
        if(proVersion) PlayerPrefs.SetInt("coins", 2000);
        PlayerPrefs.SetInt("vibrate", 0);
        PlayerPrefs.SetFloat("musicVolume", 0.8f);
        PlayerPrefs.SetFloat("fxVolume", 0.8f);
        PlayerPrefs.SetInt("showCoins", 1);
        yield return new WaitForSeconds(2);
        initialTextScript.gameObject.SetActive(true);
        yield return StartCoroutine(initialTextScript.TypeAll());
        StartCoroutine(NormalRun());
    }

    IEnumerator NormalRun()
    {
        yield return new WaitForSeconds(2);
        buttonsAnim.SetTrigger("appear");
        yield return new WaitForSeconds(1f);
        buttonsChildAnim.SetTrigger("appearChildren");
    }
}

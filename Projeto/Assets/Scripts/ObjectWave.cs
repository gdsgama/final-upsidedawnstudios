﻿using System.Collections;
using UnityEngine;

public class ObjectWave : MonoBehaviour
{
    public float minY;
    public float maxY;
    public GameObject[] objects;
    public float delayBeetweenspawns;
    public float delayBeetWeenWaves;
    public int numOberOfWaves;
    public int xSpawnPosition;
    public float shiftSpeed;
    public float spaceBetweenMeteors;
    public float tempoAtivo;
    public Vector2 targetAspectRatio; //aspectRatio


    int objCounter;

    void Start()
    {
        StartCoroutine(WaveSpawn());
    }


    IEnumerator WaveSpawn()
    {
        bool direction = true;
        for (uint j = 0; j < numOberOfWaves; j++)
        {
            StartCoroutine(Wave(direction));
            yield return new WaitForSeconds(delayBeetWeenWaves);
            direction ^= true;
        }
    }
    IEnumerator Wave(bool direction)
    {
        int directionVal = (direction ? 1 : -1);
        float startPos = (direction ? minY : maxY);
        for (int l = 0; l < objects.Length; l++)
        {
            Vector3 pos = Vector3.zero;
            pos.x = xSpawnPosition/*(xSpawnPosition * Camera.main.aspect) / targetAspect*/;
            pos.y = startPos+ spaceBetweenMeteors * l * directionVal;
            GameObject obj = Instantiate(objects[l], pos, Quaternion.identity);
            obj.GetComponent<Rigidbody2D>().velocity = new Vector3(-shiftSpeed, 0, 0);
            StartCoroutine(despawnWithTime(obj, tempoAtivo));
            yield return new WaitForSeconds(delayBeetweenspawns);
        }
    }

    IEnumerator despawnWithTime(GameObject obj, float sec)
    {
        yield return new WaitForSeconds(sec);
        obj.transform.position = Vector3.right * 1000;
        Destroy(obj);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class followObject : MonoBehaviour
{
    public GameObject obj;

    void Update()
    {
        this.transform.position = obj.transform.position;
    }
}

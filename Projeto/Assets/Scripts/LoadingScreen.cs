﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreen : MonoBehaviour
{
    public bool harcore;

    public string sceneName;

    public GameObject loadingSpriteObj;
    public Image tut1;
    public Image tut2;
    public Image tut3;
    public AudioSource source;
    public AudioClip clickSound;

    bool tutorialDone, flag;
    uint clickNum = 0;

    private void Start()
    {
        if (!harcore)
        {
            flag = true;
            if (PlayerPrefs.GetInt("Tutorial") == 0)
            {
                PlayerPrefs.SetInt("Tutorial", 1);
                tut1.enabled = true;
                tutorialDone = false;
                loadingSpriteObj.SetActive(false);
            }
            else
            {
                tut1.enabled = false;
                tut2.enabled = false;
                tut3.enabled = false;
                tutorialDone = true;
            }
        }
    }
    private void Update()
    {
        if (harcore)
        {
            SceneManager.LoadSceneAsync("HardcoreGame");
            harcore = false;
            return;
        }
        if (flag)
        {
            if (tutorialDone)
            {
                Debug.Log("tutorialDone");

                loadingSpriteObj.SetActive(true);
                flag = false;
                SceneManager.LoadSceneAsync(sceneName);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                clickNum++;
                if (clickNum == 1)
                {
                    tut1.enabled = false;
                    tut2.enabled = true;
                }
                else if (clickNum == 2)
                {
                    tut2.enabled = false;
                    tut3.enabled = true;
                }
                else
                {
                    tut3.enabled = false;
                    tutorialDone = true;
                }
                source.PlayOneShot(clickSound);
                Debug.Log("++");
            }
        }
    }

}
﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HardcoreMovement : MonoBehaviour
{
    public uint shield;

    [Header("Movement values")]
    public Vector2 forceDown; // quantidade de força do salto para baixo  
    public Vector2 forceUp; // quantidade de força do salto para cima
    public Vector2 forceBack;
    public float boostThrust; // força extra aplicada a depois de se manter premido e largar

    public Rigidbody2D rb; // rigidbody do player

    [Header("Audio Clips")]
    public AudioClip tapUpSound; // som do tap up
    public AudioClip tapDownSound; // som do tap down
    public AudioClip thrustIddle;
    public AudioClip deathSound;
    public AudioClip shieldSound;
    public AudioClip chargeSound;
    public AudioClip releaseSound;

    [Header("Audio Source")]
    public AudioSource backgroundPlayer;
    public AudioSource source;

    [Header("GameObjects")]
    public GameObject go; //referencia ao próprio gameObject
    public GameObject pulseObj;
    public Animator coinTextAnim;

    public Text coinsText;

    float nonAccuratechargeTime;
    float diference;
    bool chargeSoundCond;
    uint cont = 0;
    bool ability;
    bool coinsShow;
    bool vibrate;
    bool touchedDown; //boolean que determina se o click foi na parte de baixo do ecra ou nao
    Animator anim; //variavel que guarda o controlador das animações

    void Start()
    {
        //Speed

        ability = false;
        shield = 0;
        vibrate = PlayerPrefs.HasKey("vibrate");

        ////////////////CHECKS IF USER HAS UNLOCKED ABILITIES////////
        coinsShow = PlayerPrefs.GetInt("showCoins").Equals(1);
        Debug.Log(coinsShow);
        Debug.Log(PlayerPrefs.GetInt("showCoins"));
        ///////////////UPGRADES CHANGE OF VARS///////////////////////

        //Agility formula
        forceUp.y = forceUp.y + 10 * 4 * PlayerPrefs.GetInt("agility");
        forceDown.y = forceDown.y - 10 * 4 * PlayerPrefs.GetInt("agility");


        rb = rb.GetComponent<Rigidbody2D>(); // vai buscoinsar o rigidbody do jogador
        anim = GetComponent<Animator>(); //vai buscar o controlador das animaçoes

        chargeSoundCond = false;
        touchedDown = false; //dar valor inicial ao bool
        ContadorScore.tempoCorre = true;
        diference = 0;

        source.clip = thrustIddle;
        source.loop = true;
        source.Play();
    }

    void Update()
    {
        if (pausaMenu.gameIsPaused == 0)
        {
            if (Input.GetMouseButtonUp(0))
            {
                    rb.velocity *= 0.2f;
                    if (touchedDown) //se se tocou com o rato na parte de baixo
                    {
                        rb.AddForce(forceDown); // adiciona força forceUp declarada no editor
                        anim.SetTrigger("TapDown"); //aciona a animação de descer
                        source.PlayOneShot(tapDownSound);
                    }
                    else
                    { // igual mas no caso de se ter tocado na parte de cima do ecra
                        rb.AddForce(forceUp);
                        anim.SetTrigger("TapUp"); //aciona a animação de subir
                        source.PlayOneShot(tapUpSound);
                    }
            }
            if (Input.GetMouseButton(0)) //se o butao ESTIVER A SER primido
            {
                if (Input.touchCount == 1)
                {
                    float touchPosY = Input.GetTouch(0).position.y;
                   
                    if (touchPosY > Screen.height / 2)
                    {
                        touchedDown = false;
                    }
                    else
                    {
                        touchedDown = true;
                    }
                }
                else if (!ability)
                {
                    if (Input.touchCount == 2 && shield>0)
                    {
                        ability = true;
                        shield--;
                        if (vibrate) Vibrator.Vibrate(18);
                        if (!pulseObj.activeInHierarchy)
                        {
                            pulseObj.SetActive(true);
                            //enables god mode
                            gameObject.layer = 17;
                            source.PlayOneShot(shieldSound);
                            StartCoroutine(waitEndOf(pulseObj, 1));
                        }
                    }
                }
            }
            else
            {
                diference = 0;
            }
        }
    }


    //por agora, destroi o objeto se detetar colisao, mas futuramente correr uma animaçao de morte e fazer transiçao para uma especie de endGameScene
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstaculo") //se colidir com obstaculo
        {
            if (cont == 0)
            {
                StartCoroutine(waitForDeathAnimation());
                cont++;
            }

        }
    }
    public void cointHit(GameObject obj)
    {
        StartCoroutine(waitToEnable(obj));
    }
    public IEnumerator waitToEnable(GameObject obj)
    {
        yield return new WaitForSeconds(.1f);
        obj.SetActive(false);
        if (coinsShow)
        {
            coinTextAnim.SetTrigger("coin");
            coinsText.text = PlayerPrefs.GetInt("coins").ToString();
        }
        yield return new WaitForSeconds(4);
        obj.SetActive(true);
        obj.GetComponent<SpriteRenderer>().enabled = true;
    }

    public IEnumerator waitForDeathAnimation()
    {
        Vibrator.Vibrate(60);
        ContadorScore.tempoCorre = false;
        rb.velocity = Vector2.zero;
        rb.AddForce(forceBack + new Vector2(ContadorScore.getScore() * 0.1f, 0));
        rb.constraints = RigidbodyConstraints2D.FreezePositionY;
        backgroundPlayer.Stop();
        source.Stop();
        anim.SetTrigger("Explode");
        source.PlayOneShot(deathSound);
        yield return new WaitForSeconds(1);
        int score = ContadorScore.getScore(); //vai buscar o score
        if (score > PlayerPrefs.GetInt("Highscore", 0))
        { //ver se é maior que o highscore
            PlayerPrefs.SetInt("Highscore", score); //se sim, highscore é o novo score
            PlayerPrefs.SetInt("BonusDistance", PlayerPrefs.GetInt("BonusDistance") + score);       //incrementa o código para o boost
        }
        PlayerPrefs.SetInt("LatestScore", score);
        ContadorScore.resetScore(); //score volta a zero
        Time.timeScale = 1;
        SceneManager.LoadScene("EndGame"); // vai para a endGameScene
    }

    IEnumerator waitEndOf(GameObject obj, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        obj.SetActive(false);
        //disable god mode
        gameObject.layer = 10;
        ability = false;
    }

}

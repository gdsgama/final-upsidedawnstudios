﻿using System.Collections;

using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{


    public void ComecarJogo()
    {
        SceneManager.LoadScene("Loading"); // Para comecar o jogo
    }
    public void BackToMenu()
    {
        MainMenu.back = 1;
        SceneManager.LoadScene("Menu");
    }
    public void Exit()
    {
        Application.Quit();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradePlusButton : MonoBehaviour
{
    public static string agilityDescription = "The droid's speed of dodging obstacles";
    public static string batteryDescription = "The droid's battery efficiency";
    public static string speedDescription = "The droid's base velocity";
    public static int baseCost = 100;

    public Image lvlBar;
    public int lvlAgility;
    public int lvlSpeed;
    public int lvlBattery;
    public GameObject confirmBttnObj;

    Text description;
    Text cost;
    Button bttn;

    [Header("LevelImages")]
    public Sprite lvl0;
    public Sprite lvl1;
    public Sprite lvl2;
    public Sprite lvl3;
    public Sprite lvl4;
    public Sprite lvl5;


    void Start()
    {
        description = GameObject.Find("Description").GetComponent<Text>();
        cost = GameObject.Find("Cost").GetComponent<Text>();
        bttn = GetComponent<Button>();

        if (gameObject.name.Equals("AgilityButton"))
        {
            if (PlayerPrefs.HasKey("agility"))
            {
                lvlAgility = PlayerPrefs.GetInt("agility");
                setLevel(lvlAgility);
            }
            else
            {
                lvlAgility = 0;
                PlayerPrefs.SetInt("agility", 0);
            }
        }
        else if (gameObject.name.Equals("SpeedButton"))
        {
            if (PlayerPrefs.HasKey("speed"))
            {
                lvlSpeed = PlayerPrefs.GetInt("speed");
                setLevel(lvlSpeed);
            }
            else
            {
                lvlSpeed = 0;
                PlayerPrefs.SetInt("speed", 0);
            }
        }
        else if (gameObject.name.Equals("BatteryButton"))
        {
            if (PlayerPrefs.HasKey("battery"))
            {
                lvlBattery = PlayerPrefs.GetInt("battery");
                setLevel(lvlBattery);
            }
            else
            {
                lvlBattery = 0;
                PlayerPrefs.SetInt("battery", 0);
            }
        }
    }
    public void preLvlUp()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        if (gameObject.name.Equals("AgilityButton"))
        {
            Debug.Log("agility");
            description.text = agilityDescription;
        }
        else if (gameObject.name.Equals("SpeedButton"))
        {
            description.text = speedDescription;
        }
        else if (gameObject.name.Equals("BatteryButton"))
        {
            description.text = batteryDescription;
        }
        cost.text = "Cost------" + obtainCost();
        confirmBttnObj.SetActive(true);

        UpgradeMenuConfirmAction script = confirmBttnObj.GetComponent<UpgradeMenuConfirmAction>();
        script.lastActioObjName = gameObject.name;
        script.actionCost = obtainCost();
    }

    private int obtainCost()
    {
        int lvl;
        if (gameObject.name.Equals("AgilityButton"))
        {
            lvl = lvlAgility;
        }
        else if (gameObject.name.Equals("SpeedButton"))
        {
            lvl = lvlSpeed;
        }
        else
        {
            lvl = lvlBattery;
        }
        return (int)(Mathf.Pow(lvl, 2) * baseCost) + baseCost;
    }

    public void levelUp()
    {
        if (gameObject.name.Equals("AgilityButton"))
        {
            lvlAgility++;
            PlayerPrefs.SetInt("agility", lvlAgility);
            setLevel(lvlAgility);
        }
        else if (gameObject.name.Equals("SpeedButton"))
        {
            lvlSpeed++;
            PlayerPrefs.SetInt("speed", lvlSpeed);
            setLevel(lvlSpeed);
        }
        else if (gameObject.name.Equals("BatteryButton"))
        {
            lvlBattery++;
            PlayerPrefs.SetInt("battery", lvlBattery);
            setLevel(lvlBattery);
        }
        description.text = "";
        cost.text = "";
    }

    public void setLevel(int level)
    {
        switch (level)
        {
            case 0:
                lvlBar.sprite = lvl0;
                break;
            case 1:
                lvlBar.sprite = lvl1;
                break;
            case 2:
                lvlBar.sprite = lvl2;
                break;
            case 3:
                lvlBar.sprite = lvl3;
                break;
            case 4:
                lvlBar.sprite = lvl4;
                break;
            case 5:
                lvlBar.sprite = lvl5;
                bttn.interactable = false;
                break;
        }
    }
}

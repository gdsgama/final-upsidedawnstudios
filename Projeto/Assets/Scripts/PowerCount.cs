﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PowerCount : MonoBehaviour
{
    public static uint powerCount;
    public Text powerCountText;
    void Start()
    {
        powerCount = 0;
    }

    private void FixedUpdate()
    {
        powerCountText.text = powerCount + "x";
    }

}
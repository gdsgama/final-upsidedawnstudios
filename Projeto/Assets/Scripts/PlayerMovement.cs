﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public static float timeScaleSpeed;

    [Header("Is this hardcore mode?")]
    public bool hardcore;
    [Header("Movement values")]
    public Vector2 forceDown; // quantidade de força do salto para baixo  
    public Vector2 forceUp; // quantidade de força do salto para cima
    public Vector2 forceBack;
    public float boostThrust; // força extra aplicada a depois de se manter premido e largar

    public Rigidbody2D rb; // rigidbody do player

    [Header("Audio Clips")]
    public AudioClip tapUpSound; // som do tap up
    public AudioClip tapDownSound; // som do tap down
    public AudioClip thrustIddle;
    public AudioClip deathSound;
    public AudioClip shieldSound;
    public AudioClip chargeSound;
    public AudioClip releaseSound;

    [Header("Audio Source")]
    public AudioSource backgroundPlayer;
    public AudioSource source;

    [Header("GameObjects")]
    public GameObject go; //referencia ao próprio gameObject
    public GameObject pulseObj;
    public Animator coinTextAnim;

    public Text coinsText;

    float nonAccuratechargeTime;
    float diference;
    bool chargeSoundCond;
    uint cont = 0;
    bool hasShield;
    bool ability;
    bool coinsShow;
    bool vibrate;
    bool touchedDown; //boolean que determina se o click foi na parte de baixo do ecra ou nao
    Animator anim; //variavel que guarda o controlador das animações

    void Start()
    {
        //Speed
        timeScaleSpeed = .7f + .08f * PlayerPrefs.GetInt("speed");
        Time.timeScale = timeScaleSpeed;

        ability = false;

        vibrate = PlayerPrefs.HasKey("vibrate");

        ////////////////CHECKS IF USER HAS UNLOCKED ABILITIES////////
        hasShield = PlayerPrefs.HasKey("shield");
        coinsShow = PlayerPrefs.GetInt("showCoins").Equals(1);
        Debug.Log(coinsShow);
        Debug.Log(PlayerPrefs.GetInt("showCoins"));
        ///////////////UPGRADES CHANGE OF VARS///////////////////////

        //Agility formula
        forceUp.y = forceUp.y + 10 * 4 * PlayerPrefs.GetInt("agility");
        forceDown.y = forceDown.y - 10 * 4 * PlayerPrefs.GetInt("agility");


        rb = rb.GetComponent<Rigidbody2D>(); // vai buscoinsar o rigidbody do jogador
        anim = GetComponent<Animator>(); //vai buscar o controlador das animaçoes

        chargeSoundCond = false;
        touchedDown = false; //dar valor inicial ao bool
        ContadorScore.tempoCorre = true;
        diference = 0;

        source.clip = thrustIddle;
        source.loop = true;
        source.Play();
    }

    void Update()
    {
        if (pausaMenu.gameIsPaused == 0)
        {
            if (Input.GetMouseButtonUp(0))
            {
                // Quando o rato é largado
                nonAccuratechargeTime = diference; //copia o valor do valor do tempo do butao primido que foi obtido em tempo real
                diference = 0; // faz reset ao tempo primido para nao acumular
                               ////chargeTime = Time.time - mouseDownTime; // calcula o tempo em que o rato esteve a ser premido
                if (nonAccuratechargeTime <= 0.3f) // se foi um click simples e nao foi premido durante muito tempo (vai buscar a velocidade em tempo real)
                {
                    rb.velocity *= 0.2f;
                    if (touchedDown) //se se tocou com o rato na parte de baixo
                    {
                        rb.AddForce(forceDown); // adiciona força forceUp declarada no editor
                        anim.SetTrigger("TapDown"); //aciona a animação de descer
                        source.PlayOneShot(tapDownSound);
                    }
                    else
                    { // igual mas no caso de se ter tocado na parte de cima do ecra
                        rb.AddForce(forceUp);
                        anim.SetTrigger("TapUp"); //aciona a animação de subir
                        source.PlayOneShot(tapUpSound);
                    }
                }
                else
                { // se o rato foi premido durante mais de x segundos
                    if (nonAccuratechargeTime > 0.9f)
                    { // limite à força que pode ser aplicada
                        nonAccuratechargeTime = 0.9f; //se passar o limite volta ao limite
                    }
                    rb.velocity *= 0.5f;
                    if (touchedDown)
                    { // se tocou na metade de baixo
                        rb.AddForce(forceDown * nonAccuratechargeTime * boostThrust); //aplica força, quanto maior foi o chargeTime e multiplicado pelo boosThrust declarado no editor, para poder controlar a força
                        anim.SetTrigger("ReleaseDown");
                    }
                    else
                    { // igual mas no caso de ser ter tocado na parte de cima
                        rb.AddForce(forceUp * nonAccuratechargeTime * boostThrust);
                        anim.SetTrigger("ReleaseUp");
                    }
                    source.Stop();
                    chargeSoundCond = false;
                    source.PlayOneShot(releaseSound);
                    if (vibrate) Vibrator.Vibrate(8);
                    anim.ResetTrigger("Charge");

                }
            }
            if (Input.GetMouseButton(0)) //se o butao ESTIVER A SER primido
            {
                if (Input.touchCount == 1)
                {
                    float touchPosY = Input.GetTouch(0).position.y;
                    diference += Time.deltaTime;
                    if (diference > 0.3f)
                    {
                        if (!chargeSoundCond)
                        {
                            source.PlayOneShot(chargeSound);
                            chargeSoundCond = true;
                        }

                        anim.SetTrigger("Charge"); //aciona a animacao charge
                    }
                    //isto vai ver se o jogo esta pausado para ter a certeza se o time scale vai ser nulo ou normal

                    if (touchPosY > Screen.height / 2)
                    {
                        touchedDown = false;
                    }
                    else
                    {
                        touchedDown = true;

                    }
                }
                else if (!ability && Input.touchCount == 2)
                {
                    if ((hardcore && PowerCount.powerCount > 0) || (!hardcore && hasShield))
                    {
                        if (hardcore) PowerCount.powerCount--;
                        ability = true;
                        if (vibrate) Vibrator.Vibrate(18);
                        if (!pulseObj.activeInHierarchy)
                        {
                            pulseObj.SetActive(true);
                            //enables god mode
                            gameObject.layer = 17;
                            source.PlayOneShot(shieldSound);
                            StartCoroutine(waitEndOf(pulseObj, 1));
                        }
                    }
                }
            }
            else
            {
                diference = 0;
            }
        }
    }


    //por agora, destroi o objeto se detetar colisao, mas futuramente correr uma animaçao de morte e fazer transiçao para uma especie de endGameScene
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstaculo") //se colidir com obstaculo
        {
            if (cont == 0)
            {
                StartCoroutine(waitForDeathAnimation());
                cont++;
            }

        }
    }
    public void cointHit(GameObject obj)
    {
        StartCoroutine(waitToEnable(obj));
    }
    public IEnumerator waitToEnable(GameObject obj)
    {
        yield return new WaitForSeconds(.1f);
        obj.SetActive(false);
        if (coinsShow)
        {
            coinTextAnim.SetTrigger("coin");
            coinsText.text = PlayerPrefs.GetInt("coins").ToString();
        }
        yield return new WaitForSeconds(4);
        obj.SetActive(true);
        obj.GetComponent<SpriteRenderer>().enabled = true;
    }

    public IEnumerator waitForDeathAnimation()
    {
        Vibrator.Vibrate(60);
        ContadorScore.tempoCorre = false;
        rb.velocity = Vector2.zero;
        rb.AddForce(forceBack + new Vector2(ContadorScore.getScore() * 0.1f, 0));
        rb.constraints = RigidbodyConstraints2D.FreezePositionY;
        backgroundPlayer.Stop();
        source.Stop();
        anim.SetTrigger("Explode");
        source.PlayOneShot(deathSound);
        yield return new WaitForSeconds(1);
        int score = ContadorScore.getScore(); //vai buscar o score
        if (!hardcore)
        {
            if (score > PlayerPrefs.GetInt("Highscore", 0))
            { //ver se é maior que o highscore
                PlayerPrefs.SetInt("Highscore", score); //se sim, highscore é o novo score
            }
            PlayerPrefs.SetInt("BonusDistance", PlayerPrefs.GetInt("BonusDistance") + score);       //incrementa o código para o boost
        }
        else
        {
            PlayerPrefs.SetInt("hardcore", 0);
        }
        PlayerPrefs.SetInt("LatestScore", score);
        ContadorScore.resetScore(); //score volta a zero
        Time.timeScale = 1;
        GoogleAdController.loadStaticAd();
        GoogleAdController.display = true;
        SceneManager.LoadScene("EndGame"); // vai para a endGameScene
    }

    IEnumerator waitEndOf(GameObject obj, int seconds)
    {
        yield return new WaitForSeconds(seconds);
        obj.SetActive(false);
        //disable god mode
        gameObject.layer = 10;
        ability = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ExploreMenu : MonoBehaviour
{
    public AudioSource source;
    public AudioClip noClip;
    public AudioClip yesClip;
    public GameObject obj;
    public GameObject planetsObj;
    public GameObject obj2;
    public Image[] planets;
    public TypeWriterCommon planetTyper;
    public GoogleAdController adController;
    [Multiline]
    public string[] planetsMessages;

    [Multiline]
    public string lockedMessage;

    private float lastPos;
    void Start()
    {
        GoogleAdController.menusClicks++;
        adController.displayStaticAd();
        for (int i = 0; i < planets.Length; i++)
        {
            planets[i].GetComponent<Canvas>().sortingOrder = 3;
            if (!PlayerPrefs.HasKey("planet" + (i + 1)))
                planets[i].color = new Color(0, 0, 0, 190);
        }
    }

    void Update()
    {
        if (Input.touchCount == 1)
        {

            Touch touch = Input.GetTouch(0);
            if (touch.phase.Equals(TouchPhase.Began) || touch.phase.Equals(TouchPhase.Stationary))
            {
                lastPos = touch.position.x;
            }
            else if (touch.phase.Equals(TouchPhase.Moved))
            {
                float dif = touch.position.x - lastPos;
                Debug.Log(dif);
                if ((obj.transform.position.x > -11145 && dif < 0))
                {
                    Debug.Log(obj.transform.position.x + "dir1");
                    Debug.Log("dir");
                }
                if ((obj.transform.position.x > -150f && dif < 0) || (obj.transform.position.x < 0 && dif > 0))
                {
                    float logDif = (float)System.Math.Log(dif);
                    if (dif < 0)
                        logDif = -(float)System.Math.Log(-dif);
                    obj.transform.position = new Vector3(obj.transform.position.x + (logDif / 8), obj.transform.position.y, obj.transform.position.z);
                    planetsObj.transform.position = new Vector3(planetsObj.transform.position.x + (logDif / 10), planetsObj.transform.position.y, planetsObj.transform.position.z);
                    obj2.transform.position = new Vector3(obj2.transform.position.x + (logDif / 12), obj2.transform.position.y, obj2.transform.position.z);
                }
            }
        }
    }
    public void backClick()
    {
        MainMenu.back = 1;
        SceneManager.LoadScene("Menu");
    }
    public void planetClick(int index)
    {
        if (!PlayerPrefs.HasKey("planet" + index))
        {
            source.PlayOneShot(noClip);
            if (!planetTyper.isTyping) StartCoroutine(planetTyper.Type(lockedMessage));
        }
        else
        {
            source.PlayOneShot(yesClip);
            if (!planetTyper.isTyping) StartCoroutine(planetTyper.Type(planetsMessages[index-1]));
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpHardcore : MonoBehaviour
{
    public AudioClip clip;
    public Animator anim;
    bool disable;
    AudioSource source;
    void Start()
    {
        source = GetComponent<AudioSource>();
        source.volume = PlayerPrefs.GetFloat("fxVolume");
        disable = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!disable)
        {
            disable = true;
            anim.SetTrigger("powerUp");
            source.PlayOneShot(clip);
            PowerCount.powerCount++;
            StartCoroutine(disableFlag());
        }
    }

    IEnumerator disableFlag()
    {
        yield return new WaitForSeconds(2f);
        disable = false;
    }
}
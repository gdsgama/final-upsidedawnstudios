﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour
{
    public float fuelDecrease;
    private void OnEnable()
    {
        Fuel.fuel -= fuelDecrease;
    }
}

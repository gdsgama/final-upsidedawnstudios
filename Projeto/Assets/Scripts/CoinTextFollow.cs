﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteInEditMode]
public class CoinTextFollow : MonoBehaviour
{
    public static float heightOffSet=1.5f;
    public  RectTransform playerTransform;

    float xPos;
    RectTransform localTransform;

    private void Start()
    {
        localTransform = GetComponent<RectTransform>();
        xPos = localTransform.position.x;
    }
    void Update()
    {
        localTransform.position = new Vector3(xPos, playerTransform.position.y+heightOffSet);
    }
}

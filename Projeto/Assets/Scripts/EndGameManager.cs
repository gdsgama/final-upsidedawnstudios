﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGameManager : MonoBehaviour
{
    public GameObject normalCanvas;
    public GameObject boostCanvas;
    public GameObject popUpCanvas;
    public Text coinsTxt;
    public GameObject failureText;
    public GameObject winText;
    public GoogleAdController controllerAd;
    public static int bonusDistance = 1900;

    void Start()
    {
        controllerAd.displayStaticAd();
        if (PlayerPrefs.GetInt("endingText", 0) == 1)
        {
            PlayerPrefs.SetInt("endingText", 0);
            failureText.SetActive(false);
            winText.SetActive(true);
        }
        else
        {
            winText.SetActive(false);
        }
        normalCanvas.SetActive(true);
        coinsTxt.text = PlayerPrefs.GetInt("coins").ToString();
        if (PlayerPrefs.GetInt("BonusDistance") > bonusDistance)
        {
            if (!MainMenu.proVersion)
            {

            GoogleAdController.loadRewardedAd();
            popUpCanvas.SetActive(true);
            PlayerPrefs.SetInt("BonusDistance", PlayerPrefs.GetInt("BonusDistance") - bonusDistance);
            }
            else
            {
                yesClick();
            }
        }
    }
    public void relaunchClick()
    {
        if (PlayerPrefs.HasKey("hardcore"))
        {
            PlayerPrefs.DeleteKey("hardcore");
            SceneManager.LoadScene("LoadingHardcore");
        }
        else
        {
            SceneManager.LoadScene("Loading");
        }
    }
    public void yesClick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        Debug.Log("yes");
        normalCanvas.SetActive(false);
        boostCanvas.SetActive(true);
        popUpCanvas.SetActive(false);
    }
    public void noClick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        Debug.Log("no");
        popUpCanvas.SetActive(false);
    }
    public void workshopClick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        Vibrator.Vibrate(8);
        SceneManager.LoadScene("UpgradeShop");
    }
    public void shopClick()
    {
        if (PlayerPrefs.HasKey("vibrate")) Vibrator.Vibrate(8);
        Vibrator.Vibrate(8);
        SceneManager.LoadScene("PurchasesMenu");
    }

}

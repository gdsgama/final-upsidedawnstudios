﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HarcoreManager : MonoBehaviour
{
    public float speed;
    public GameObject PulseObj;

    private void Start()
    {
        StartCoroutine(speedUpGame());
    }

    IEnumerator speedUpGame()
    {
        yield return new WaitForSeconds(.1f);
        Time.timeScale = speed;
    }

    IEnumerator disablePulse()
    {
        yield return new WaitForSeconds(.1f);
        PulseObj.SetActive(false);
    }



   

}

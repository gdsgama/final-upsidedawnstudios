﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoostSort : MonoBehaviour
{
    public float maxTime;
    public Text targetText;
    public AudioSource source;
    public AudioClip sortClip;
    public GameObject closeButton;

    private float roulleteTime = 0;
    private int version = 1;
    private int realSortedNumber;

    void Start()
    {
        realSortedNumber = sortWithChances(800, 5000)/100;      //to make sure the sorted number ends on 00 ex:1500 or 800 and not 965 or 960
        realSortedNumber *= 100;
        PlayerPrefs.SetInt("boost", realSortedNumber);
        StartCoroutine(numberRoulleteAnim());
    }

    IEnumerator numberRoulleteAnim()
    {
        if (roulleteTime < maxTime / 2)
        {
            yield return new WaitForSeconds(0.05f);
            source.PlayOneShot(sortClip);
            targetText.text = Random.Range(0, 9999).ToString();
            roulleteTime += 0.05f;
            StartCoroutine(numberRoulleteAnim());
        }
        else if (roulleteTime <= maxTime)
        {
            yield return new WaitForSeconds((float)0.05 * version);
            source.PlayOneShot(sortClip);
            targetText.text = Random.Range(0, 9999).ToString();
            roulleteTime += (float)0.05 * version;
            version++;
            StartCoroutine(numberRoulleteAnim());
        }
        else
        {
            source.PlayOneShot(sortClip);
            targetText.text = realSortedNumber.ToString();
            closeButton.SetActive(true);
        }
    }
    private int sortWithChances(int min, int max)
    {
        int[] randomPool = new int[4];
        randomPool[0] = Random.Range(min, max / 4);                 //  1/4 ->    52% chance
        randomPool[1] = Random.Range(min, max / 2);                 //  2/4 ->    27% chance
        randomPool[2] = Random.Range(min, max * 3 / 4);             //  3/4 ->    12,6% chance
        randomPool[3] = Random.Range(min, max);                     //  4/4 ->    6,3% chance
        int i = Random.Range(0, randomPool.Length);
        return randomPool[i];
    }

}

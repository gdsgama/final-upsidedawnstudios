﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class upgradeMenuManager : MonoBehaviour
{
    public GoogleAdController adController;
    public GameObject normalCanvas;
    public GameObject popUpCanvas;

    void Start()
    {
        GoogleAdController.menusClicks++;
        adController.displayStaticAd();
        normalCanvas.SetActive(true);
        popUpCanvas.SetActive(false);
    }

    public void relaunchClick()
    {
        SceneManager.LoadScene("Loading");
    }

    public void menuhClick()
    {
        SceneManager.LoadScene("Menu");
    }

    public void preExitClick()
    {
        //normalCanvas.SetActive(false);
        popUpCanvas.SetActive(true);
    }

}

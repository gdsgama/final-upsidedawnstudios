﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulbEffect : MonoBehaviour
{
    public AudioClip[] clips;
    public AudioSource source;
    Animator anim;
    int i;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        StartCoroutine(Bulb());
        i = 0;
    }
    IEnumerator Bulb()
    {
        yield return new WaitForSeconds(Random.Range(1.8f, 4));
        anim.SetTrigger("bulb");
        yield return new WaitForSeconds(0.2f);
        if (i>= clips.Length)
        {
            i = 0;
        }
        source.PlayOneShot(clips[i]);
        i++;
        StartCoroutine(Bulb());
    }
}
